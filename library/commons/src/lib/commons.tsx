import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface CommonsProps {}

const StyledCommons = styled.div`
  color: pink;
`;

export function Commons(props: CommonsProps) {
  return (
    <StyledCommons>
      <h1>Welcome to Commons!</h1>
    </StyledCommons>
  );
}

export default Commons;
