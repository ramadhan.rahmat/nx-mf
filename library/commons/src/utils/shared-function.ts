export const isClient = (): boolean =>
  typeof window !== 'undefined' || typeof document !== 'undefined';

export const isOnline = (): boolean => isClient() && window.navigator.onLine;
