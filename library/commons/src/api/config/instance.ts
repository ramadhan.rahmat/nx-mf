import { toast } from 'react-toastify';
import axios, {
  AxiosResponse,
  InternalAxiosRequestConfig as AxiosRequest,
} from 'axios';
import { isOnline } from '../../utils/shared-function';

interface IConfigInstance {
  headers?: Record<string, string | undefined>;
  reqInterceptor?: (req: AxiosRequest) => AxiosRequest | Promise<AxiosRequest>;
  reqErrorHandler?: (error: any) => any;
  resInterceptor?: (
    res: AxiosResponse
  ) => AxiosResponse | Promise<AxiosResponse>;
  resErrorHandler?: (error: any) => any;
}

const buildApiInstance = (
  baseUrl: string | undefined,
  config?: IConfigInstance
) => {
  if (typeof baseUrl === 'undefined' || typeof baseUrl !== 'string') {
    throw new Error('You must provide valid baseURL');
  }

  const headers = config?.headers || {};
  const instance = axios.create({
    baseURL: baseUrl,
    headers: {
      'Content-Type': 'application/json',
      'X-Platform': 'desktop',
      ...headers,
    },
    // TODO: temporary disable until further improvement on adapter
    // adapter: window?.__SDA__?.update_channel === 'production' ? tauriHttpAdapter : undefined,
  });

  instance.interceptors.request.use((req) => {
    if (!isOnline()) {
      // use library toast instead of @utils/toast to avoid .dismiss proxy
      toast.warning('No Internet Connection', {
        toastId: 'no-internet',
      });
      return Promise.reject(new Error('No Internet Connection'));
    }

    if (config?.reqInterceptor) return config.reqInterceptor(req);

    return req;
  }, config?.reqErrorHandler);
  instance.interceptors.response.use(
    config?.resInterceptor,
    config?.resErrorHandler
  );

  return instance;
};

export default buildApiInstance;
