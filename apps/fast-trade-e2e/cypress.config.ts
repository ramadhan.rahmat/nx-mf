import { nxE2EPreset } from '@nx/cypress/plugins/cypress-preset';

import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    ...nxE2EPreset(__filename, {
      cypressDir: 'src',
      webServerCommands: {
        default: 'nx run fast-trade:serve',
        production: 'nx run fast-trade:preview',
      },
      ciWebServerCommand: 'nx run fast-trade:serve-static',
    }),
    baseUrl: 'http://localhost:4200',
  },
});
