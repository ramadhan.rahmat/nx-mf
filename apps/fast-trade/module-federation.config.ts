import { ModuleFederationConfig } from '@nx/webpack';

const config: ModuleFederationConfig = {
  name: 'fast-trade',

  exposes: {
    './Module': './src/remote-entry.ts',
  },
};

export default config;
